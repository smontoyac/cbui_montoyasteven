package sample.tl;

import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ControllerFx implements Initializable {

    @FXML
    private VBox vbox;
    private Parent fxml;

    @FXML
    private ImageView imgMain;

    @FXML
    private ImageView imgExit;

    @FXML
    private ImageView imgMoney;

    @FXML
    private ImageView bntExit2;

    @FXML
    void openSignIn(ActionEvent event) {
        imgMain.setVisible(false);

        bntExit2.setVisible(false);

        TranslateTransition t = new TranslateTransition(Duration.seconds(0.7), vbox);
        t.setToX(vbox.getLayoutX() * 32);
        t.play();
        t.setOnFinished((e) -> {
            try {
                fxml = FXMLLoader.load(getClass().getResource("../views/SignIn.fxml"));
                vbox.getChildren().removeAll();
                vbox.getChildren().setAll(fxml);
                imgExit.setVisible(true);
                imgMoney.setVisible(true);
            } catch (IOException ex) {
                Logger.getLogger(ControllerFx.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    @FXML
    void openSignUp(ActionEvent event) {
        imgExit.setVisible(false);

        imgMoney.setVisible(false);

        TranslateTransition t = new TranslateTransition(Duration.seconds(0.7), vbox);
        t.setToX(14);
        t.play();
        t.setOnFinished((e) -> {
            try {
                fxml = FXMLLoader.load(getClass().getResource("../views/SignUp.fxml"));
                vbox.getChildren().removeAll();
                vbox.getChildren().setAll(fxml);
                bntExit2.setVisible(true);
                imgMain.setVisible(true);

            } catch (IOException ex) {
                Logger.getLogger(ControllerFx.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        imgMoney.setVisible(false);
        imgExit.setVisible(false);
        TranslateTransition t = new TranslateTransition(Duration.seconds(0.7), vbox);
        t.setToX(14);
        t.play();
        t.setOnFinished((e) -> {
            try {
                fxml = FXMLLoader.load(getClass().getResource("../views/SignUp.fxml"));
                vbox.getChildren().removeAll();
                vbox.getChildren().setAll(fxml);
            } catch (IOException ex) {
                Logger.getLogger(ControllerFx.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void onExitButtonClick(MouseEvent evento) {
        Platform.exit();
        System.exit(0);
    }

}