package sample.tl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class ControllerIniciarSesion {

    @FXML
    private TextField txtEmail;

    @FXML
    private PasswordField txtPassword;

    private Controller gestor = new Controller();

    public void changeScene(ActionEvent event) throws Exception {
        gestor.ingresarUsuario();
        String email = txtEmail.getText();
        String contrasenna = txtPassword.getText();

            int i = gestor.iniciarSesion(email, contrasenna);

            if (i == 0) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Collector's Bazar");
                alert.setContentText("No pudimos encontrar tu cuenta de Collector's Bazar");
                alert.showAndWait();
            }

            if (i == 1) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Collector's Bazar");
                alert.setContentText("Contraseña inconrrecta. Vuelve a intentarlo o haz click en 'Olvidó su contrseña?' para restablecerla.");
                alert.showAndWait();
            }

            if (i == 2) {
                Parent homeParent = FXMLLoader.load(getClass().getResource("../views/home.fxml"));
                Scene homeScene = new Scene(homeParent);

                //to get the stage information
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(homeScene);
                window.show();
            }
        }
    }