package sample.tl;

import com.jfoenix.controls.JFXDatePicker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.time.LocalDate;
import java.time.Period;

public class ControllerRegistrar {
    @FXML
    private TextField txtNombre;

    @FXML
    private TextField txtSegNombre;

    @FXML
    private TextField txtApellido;

    @FXML
    private TextField txtSegApellido;

    @FXML
    private TextField txtId;

    @FXML
    private JFXDatePicker txtFecha;

    @FXML
    private TextField txtEdad;

    @FXML
    private TextField txtCorreo;

    @FXML
    private PasswordField txtContrasenna;

    @FXML
    private PasswordField txtRepContrasenna;

    @FXML
    private TextField txtProvincia;

    @FXML
    private TextField txtCanton;

    @FXML
    private TextField txtDistrito;

    @FXML
    private TextField txtDireccion;

    private Period periodo;
    private LocalDate fecha;

    Controller gestor = new Controller();

    public void onDateChange(ActionEvent event){
        fecha = txtFecha.getValue();
        LocalDate ahora = LocalDate.now();
        periodo =Period.between(fecha, ahora);
        txtEdad.setText(String.valueOf(periodo.getYears()));
    }

    public void onBtnRegistrarClicked(ActionEvent event){
        int error = 0;
        String nombre = txtNombre.getText();
        String segNombre = txtSegNombre.getText();
        String apellido = txtApellido.getText();
        String segApellido = txtSegApellido.getText();
        String id = txtId.getText();
        String correo = txtCorreo.getText();
        String contrasenna = txtContrasenna.getText();
        String repContrasenna = txtRepContrasenna.getText();
        String provincia = txtProvincia.getText();
        String canton = txtCanton.getText();
        String distrito = txtDistrito.getText();
        String direccion = txtDireccion.getText();

        if (periodo != null){
            if (periodo.getYears()<18) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Collector's Bazar");
                alert.setContentText("Debes ser mayor de edad para usar la aplicación");
                alert.showAndWait();
                error = 1;
            }
        }

        if (contrasenna != null){
            if (contrasenna.equalsIgnoreCase(repContrasenna)){
                System.out.println("Contraseñas iguales");
            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Collector's Bazar");
                alert.setContentText("Las contraseñas no coinciden");
                alert.showAndWait();
                error = 1;
            }
        }

        if (nombre.isEmpty()){
            error = 2;
        }

        if (apellido.isEmpty()){
            error = 2;
        }

        if (segApellido.isEmpty()){
            error = 2;
        }

        if (id.isEmpty()){
            error = 2;
        }

        if (correo.isEmpty()){
            error = 2;
        }

        if(provincia.isEmpty()){
            error = 2;
        }

        if(canton.isEmpty()){
            error = 2;
        }

        if(distrito.isEmpty()){
            error = 2;
        }

        if(direccion.isEmpty()){
            error = 2;
        }

        if (error == 2){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Collector's Bazar");
            alert.setContentText("Por favor llene los campos obligatorios");
            alert.showAndWait();
        }else if (error == 0){
            int info = gestor.registrarPersona(nombre, segNombre, apellido, segApellido, id, fecha, periodo.getYears(), correo, contrasenna, provincia, canton, distrito, direccion);
            if (info == 1 ){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Collector's Bazar");
                alert.setContentText("Ya te has registrado en la aplicación");
                alert.showAndWait();
            }else if (info == 2){
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Collector's Bazar");
                alert.setContentText("Registro exitoso");
                alert.showAndWait();
            }
        }
    }
}
