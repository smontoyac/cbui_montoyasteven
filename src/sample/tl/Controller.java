package sample.tl;

import sample.bl.Persona;
import sample.dl.Cl;

import java.time.LocalDate;

public class Controller {
    private Cl logica;

    public Controller(){this.logica = new Cl();}

    public int registrarPersona(String nombre, String nombre2, String apellido, String apellido2, String identificacion, LocalDate fechaNacimiento, int edad, String contrasenna, String correo, String provincia, String canton, String distrito, String direccionExacta){
        Persona tmpPersona = new Persona(nombre, nombre2, apellido, apellido2, identificacion, fechaNacimiento, edad, correo, contrasenna, provincia, canton, distrito, direccionExacta, 0);
        int info = logica.ingresarPersona(tmpPersona);
        return info;
    }

    public int iniciarSesion(String email, String password){
        logica.getPersonas();
        Persona tmpPersona = new Persona();
        tmpPersona.setCorreo(email);
        tmpPersona.setContrasenna(password);
        int encontrado = logica.iniciarSesion(tmpPersona);
        return encontrado;
    }

    public void ingresarUsuario(){
        Persona tmpPersona = new Persona();
        tmpPersona.setCorreo("sss");
        tmpPersona.setContrasenna("1234");
        logica.ingresarPersona(tmpPersona);
    }
}
