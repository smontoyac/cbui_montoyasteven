package sample.dl;

import sample.bl.Persona;

import java.util.ArrayList;

public class Cl {

    private ArrayList<Persona> personas;

    public Cl() {
        personas = new ArrayList<>();
    }

    public int ingresarPersona(Persona tmpPersona) {
        int info = 0;
        if (personas.size() == 0) {
            personas.add(tmpPersona);
            info = 2;
        } else {
            for (Persona dato : getPersonas()) {
                if (dato.getCorreo().equals(tmpPersona.getCorreo())) {
                    info = 1;
                    break;
                }
            }
            if (info == 0) {
                personas.add(tmpPersona);
                info = 2;
            }
        }
        getPersonas();
        return info;
    }

    public int iniciarSesion(Persona tmpPersona) {
        int encontrado = 0;
        for (Persona dato : getPersonas()) {
            if (dato.getCorreo().equals(tmpPersona.getCorreo())) {
                encontrado = 1;
                if (dato.getContrasenna().equals(tmpPersona.getContrasenna())) {
                    encontrado = 2;
                }
            }
        }
        return encontrado;
    }

    public ArrayList<Persona> getPersonas() {
        return personas;
    }

}
